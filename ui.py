from model import BCD7Net, repackage_hidden

import random
import signal
import torch
import math
import csv
import gi

gi.require_version('Gtk', '3.0')

# Gi special imports.
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf

def train(net, input, output, target):
    net.optimizer.zero_grad()
    loss = net.criterion(output, target)
    loss.backward(retain_graph=True)
    net.optimizer.step()


class MainHandler:

    def __init__(self, builder):
        self.builder = builder
        self.input_a = builder.get_object("input_a")
        self.input_b = builder.get_object("input_b")
        self.input_c = builder.get_object("input_c")
        self.input_d = builder.get_object("input_d")
        self.input_e = builder.get_object("input_e")
        self.input_f = builder.get_object("input_f")
        self.input_g = builder.get_object("input_g")
        self.target_value = builder.get_object("target")
        self.output_0 = builder.get_object("output_0")
        self.output_1 = builder.get_object("output_1")
        self.output_2 = builder.get_object("output_2")
        self.output_3 = builder.get_object("output_3")
        self.output_4 = builder.get_object("output_4")
        self.output_5 = builder.get_object("output_5")
        self.output_6 = builder.get_object("output_6")
        self.output_7 = builder.get_object("output_7")
        self.output_8 = builder.get_object("output_8")
        self.output_9 = builder.get_object("output_9")
        self.target_value_widget = builder.get_object("train_target")
        self.visualizer = builder.get_object("visualizer")
        self.input = torch.zeros(1, 7)
        self.output = None
        self.net = BCD7Net()

    def close(self, *kargs):
        print("Closing gracefully...")
        Gtk.main_quit()

    def onInputA(self, range):
        self.input[0][0] = range.get_value()
        self.updateModelInput()

    def onInputB(self, range):
        self.input[0][1] = range.get_value()
        self.updateModelInput()

    def onInputC(self, range):
        self.input[0][2] = range.get_value()
        self.updateModelInput()

    def onInputD(self, range):
        self.input[0][3] = range.get_value()
        self.updateModelInput()

    def onInputE(self, range):
        self.input[0][4] = range.get_value()
        self.updateModelInput()

    def onInputF(self, range):
        self.input[0][5] = range.get_value()
        self.updateModelInput()

    def onInputG(self, range):
        self.input[0][6] = range.get_value()
        self.updateModelInput()

    def onTrainSave(self, button):
        with open('training_dataset.csv', 'a') as f:
            fcw = csv.writer(f)
            fcw.writerow([
                self.input_a.get_value(),
                self.input_b.get_value(),
                self.input_c.get_value(),
                self.input_d.get_value(),
                self.input_e.get_value(),
                self.input_f.get_value(),
                self.input_g.get_value(),
                int(self.target_value.get_value())
            ])

    def onTrainLoad(self, button):
        with open('training_dataset.csv', 'r') as f:
            fcr = csv.reader(f)
            self.traindataiter = 100
            self.traindataset = list(fcr)
            GLib.idle_add(self.onTrainLoadIter)

    def onTrainLoadIter(self):
        if self.traindataiter > 0:
            print(self.traindataset)
            random.shuffle(self.traindataset)
            for traindata in self.traindataset:
                input_tensor = torch.tensor([[float(i) + random.uniform(-0.1, 0.1) for i in traindata[0:7]]])
                output = self.net(input_tensor)
                target_value = int(traindata[7])
                target = torch.zeros(1, 10)
                target[0][target_value] = 1.0
                train(self.net, input_tensor, output, target)
            # Report and prepare next.
            print("Train iteration %d complete." % (100 - self.traindataiter))
            self.traindataiter = self.traindataiter - 1
            self.updateModelInput()
            return True
        else:
            del self.traindataset
            return False

    def onModelSave(self, button):
        torch.save(self.net.state_dict(), "bcd7.net")

    def onModelLoad(self, button):
        self.net.load_state_dict(torch.load("bcd7.net"))
        self.updateModelInput()

    def onVisualizationDraw(self, widget, cairo):
        # Calculate dimensions and colors.
        context = widget.get_style_context()
        width = widget.get_allocated_width()
        height = widget.get_allocated_height()
        fg_color = context.get_color(context.get_state())
        digit_width_percent = 0.05
        digit_width = width * digit_width_percent
        digit_length_percent = 0.3
        digit_length = height * digit_length_percent
        digit_width_total = digit_width * 2.0 + digit_length
        digit_x_offset = (width - digit_width_total) / 2.0
        # Clear.
        Gtk.render_background(context, cairo, 0, 0, width, height)
        # Digit display.
        # A Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][0])
        cairo.rectangle(digit_x_offset + digit_width,
                        0, digit_length, digit_width)
        cairo.fill()
        # B Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][1])
        cairo.rectangle(digit_x_offset, digit_width, digit_width, digit_length)
        cairo.fill()
        # C Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][2])
        cairo.rectangle(digit_x_offset, digit_width * 2.0 +
                        digit_length, digit_width, digit_length)
        cairo.fill()
        # D Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][3])
        cairo.rectangle(digit_x_offset + digit_width, digit_width *
                        2.0 + digit_length * 2.0, digit_length, digit_width)
        cairo.fill()
        # E Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][4])
        cairo.rectangle(digit_x_offset + digit_width + digit_length,
                        digit_width * 2.0 + digit_length, digit_width, digit_length)
        cairo.fill()
        # F Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][5])
        cairo.rectangle(digit_x_offset + digit_width +
                        digit_length, digit_width, digit_width, digit_length)
        cairo.fill()
        # G Bar:
        cairo.set_source_rgba(fg_color.red, fg_color.green,
                              fg_color.blue, fg_color.alpha * self.input[0][6])
        cairo.rectangle(digit_x_offset + digit_width,
                        digit_width + digit_length, digit_length, digit_width)
        cairo.fill()
        # TODO: Optional Neural network display.
        return False

    def updateModelInput(self):
        self.output = self.net(self.input)
        self.visualizer.queue_draw()
        normalized_output = self.output[0].clone().detach().requires_grad_(False)
        normalized_output_l2 = torch.sum(normalized_output**2)
        normalized_output = self.output[0] / normalized_output_l2
        self.output_0.set_label("0: %2.0f%%" % (normalized_output[0] * 100.0))
        self.output_1.set_label("1: %2.0f%%" % (normalized_output[1] * 100.0))
        self.output_2.set_label("2: %2.0f%%" % (normalized_output[2] * 100.0))
        self.output_3.set_label("3: %2.0f%%" % (normalized_output[3] * 100.0))
        self.output_4.set_label("4: %2.0f%%" % (normalized_output[4] * 100.0))
        self.output_5.set_label("5: %2.0f%%" % (normalized_output[5] * 100.0))
        self.output_6.set_label("6: %2.0f%%" % (normalized_output[6] * 100.0))
        self.output_7.set_label("7: %2.0f%%" % (normalized_output[7] * 100.0))
        self.output_8.set_label("8: %2.0f%%" % (normalized_output[8] * 100.0))
        self.output_9.set_label("9: %2.0f%%" % (normalized_output[9] * 100.0))
        # print(self.input[0], "->", self.output[0])

    def onTrain(self, button):
        target_value = int(self.target_value_widget.get_value())
        target = torch.zeros(1, 10)
        target[0][target_value] = 1.0
        train(self.net, self.input, self.output, target)
        self.updateModelInput()


def main():
    # Load with builder.
    main = Gtk.Builder()
    main.add_from_file('ui.glade')
    # Attach event handler.
    handler = MainHandler(main)
    main.connect_signals(handler)
    # Style
    css = Gtk.CssProvider()
    css.load_from_path("style.css")
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), css, Gtk.STYLE_PROVIDER_PRIORITY_USER)
    # Runtime UI modifications.
    handler.updateModelInput()
    # Show window.
    window = main.get_object("window")
    window.show_all()
    # UI Loop.
    signal.signal(signal.SIGINT, Gtk.main_quit)
    Gtk.main()
    # Cleanup.
    window.close()
