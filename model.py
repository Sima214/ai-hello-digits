import torch
from torch import tensor
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F


def repackage_hidden(h):
    """Wraps hidden states in new Tensors, to detach them from their history."""

    if isinstance(h, torch.Tensor):
        return h.detach()
    else:
        return tuple(repackage_hidden(v) for v in h)


class BCD7Net(nn.Module):

    def __init__(self):
        super(BCD7Net, self).__init__()
        self.fc1 = nn.Linear(7, 24)
        self.fc2 = nn.Linear(24, 18)
        self.fc3 = nn.Linear(18, 10)
        self.criterion = nn.MSELoss()
        self.optimizer = optim.SGD(self.parameters(), lr=0.08)

    def forward(self, x):
        self.last_input = x.clone().detach()
        x = torch.tanh(self.fc1(x))
        self.last_layer_0 = x.clone().detach()
        x = torch.tanh(self.fc2(x))
        self.last_layer_1 = x.clone().detach()
        x = self.fc3(x)
        self.last_output = x.clone().detach()
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
